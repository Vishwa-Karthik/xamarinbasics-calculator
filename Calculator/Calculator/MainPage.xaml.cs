﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        private decimal firstNumber;
        private string OperatorName;
        private bool isOperatorClicked;

        private void BtnCommon(object sender, EventArgs e)
        {
            var button = sender as Button;
          

            if(Result.Text == "0" || isOperatorClicked )
            {
                isOperatorClicked = false;
                Result.Text = button.Text;
            }
            else
            {
                Result.Text += button.Text;
            }
        }
        private void BtnSpcl(object sender, EventArgs e)
        {

        }
        private void ClearButton(object sender, EventArgs e)
        {
            Result.Text = "0";
        }
        private void DelButton(object sender, EventArgs e)
        {
            string number = Result.Text;

            if(number != "0")
            {
                number = number.Remove(number.Length - 1,1);

                if (string.IsNullOrEmpty(number))
                {
                    Result.Text = "0";
                }
                else
                {
                    Result.Text = number;
                }
            }
        }

        private async void percentBtn(object sender, EventArgs e)
        {
            try
            {
                string number = Result.Text;
                
                if(number != "0")
                {
                    decimal percentValue = Convert.ToDecimal(number);
                    string result = (percentValue / 100).ToString("0.##");
                    Result.Text = result;   
                }
            }
            catch(Exception ex)
            {
                await DisplayAlert("Error",ex.Message,"Try Again");
            }
        }

        private void Operator(object sender, EventArgs e)
        {
            var button = sender as Button;
            isOperatorClicked = true;
            OperatorName = button.Text;
            firstNumber = Convert.ToDecimal(Result.Text);
        }

        private async void EqualBtn(object sender, EventArgs e)
        {
            try
            {
                decimal secondNumber = Convert.ToDecimal(Result.Text);
                string finalResult = Calculate(firstNumber, secondNumber, OperatorName).ToString("0.##");
                Result.Text = finalResult;

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "Ok");
            }
        }

        private decimal Calculate(decimal firstNumber, decimal secondNumber, string operatorName)
        {
            decimal result = 0;
            
            if(operatorName == "+")
            {
                result = firstNumber + secondNumber;
            }
            if (operatorName == "-")
            {
                result = firstNumber - secondNumber;
            }
            if (operatorName == "X")
            {
                result = firstNumber * secondNumber;
            }
            if (operatorName == "/")
            {
                result = firstNumber / secondNumber;
            }
            return result;
        }
    }
}
